[![App video preview](https://firebasestorage.googleapis.com/v0/b/km50-9538e.appspot.com/o/Screen%20Shot%202018-09-14%20at%202.56.56%20PM.png?alt=media&token=4c5759e8-28cf-4a58-a9a1-5e1a96b6dc31)](https://www.youtube.com/watch?v=SC-BZubYqHE)
## Getting started
* This is an example project of Trusting Social which was developed by Swift on iOS for pre-interview purpose, please read the **TrustingSocial Pre-interview.pdf** requirement file for more details.
* You can watch a short demo video of this App here: [https://www.youtube.com/watch?v=SC-BZubYqHE](https://www.youtube.com/watch?v=SC-BZubYqHE)
## What's would you learn from this project:
1. How to apply clean convention Swift 4.2 with Xcode 10.
2. How to apply the MVC architect pattern.
3. How to use RESTful to retrieve and parse data from server.
4. How to custom an UIView drawing.
5. How to use regular expression to validate input.
6. How to apply Dependence Injection design pattern in action.
7. How to implement UI by Auto-layout & support for iPhone X, XR, XS, XS Max.
8. How to write some simple Unit Test.
9. How to use cocoapods to manage dependences.
## How to install
1. Please be ensured that the cocoapods has been installed, otherwise run a terminal command `$ sudo gem install cocoapods`
2. After cloning the source code, go to the root folder then run a terminal command: `$ pod install`
## Contact:
MR. Đào Duy Quang (Steve) | *Associated Technical Lead at FPT Software* 

* Email: daoduyquang91@gmail.com
* Skype: daoduyquang91@outlook.com

## License
###### TrustingSocial pre-interview is released under the MIT license.

###### Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

###### The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

###### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

