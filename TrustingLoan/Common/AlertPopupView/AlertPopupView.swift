//
//  AlertPopupView.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/11/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

class AlertPopupView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var btnOK: UIButton!
    
    var completion: (() -> ())?
    
    @IBAction func btnAction(sender: UIButton) {
        UIView.animate(withDuration: Constants.animationDuration, delay: 0, options: [.curveEaseInOut], animations: {
            self.alpha = 0
            self.contentView.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        }, completion: { (done) in
            self.completion?()
            self.removeFromSuperview()
        })
    }
    
    private static func loadNib() -> AlertPopupView {
        return Bundle.main.loadNibNamed(String.init(describing: AlertPopupView.self), owner: nil, options: nil)!.first! as! AlertPopupView
    }
    
    static func showAlert(withTitle title: String?, message: String?, buttonTitle: String? = "OK", completion: (() -> ())? = nil) {
        guard let window = UIWindow.applicationWindow else { return }
        let alertView = AlertPopupView.loadNib()
        DispatchQueue.main.async {
            alertView.lblTitle.text = title
            alertView.lblMessage.text = message
            alertView.btnOK.setTitle(buttonTitle, for: .normal)
            alertView.completion = completion
            alertView.frame = window.bounds
            alertView.alpha = 0
            alertView.contentView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
            window.addSubview(alertView)

            UIView.animate(withDuration: Constants.animationDuration, delay: 0, options: [.curveEaseInOut], animations: {
                alertView.alpha = 1
                alertView.contentView.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
    deinit {
        print("AlertPopupView has been released")
    }

}
