//
//  HUDView.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/11/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

class HUDView: UIView {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    
    private static func loadNib() -> HUDView {
        return Bundle.main.loadNibNamed(String.init(describing: HUDView.self), owner: nil, options: nil)?.first! as! HUDView
    }
    
    // Singleton
    private static let shared = HUDView.loadNib()
    
    static func showHUD(title: String? = "Please wait ...") {
        guard let window = UIWindow.applicationWindow else { return }
        if HUDView.shared.isDescendant(of: window) {
            DispatchQueue.main.async {
                HUDView.shared.lblTitle.text = title
            }
        } else {
            DispatchQueue.main.async {
                HUDView.shared.alpha = 0
                HUDView.shared.indicatorView.startAnimating()
                HUDView.shared.lblTitle.text = title
                HUDView.shared.frame = window.bounds
                window.addSubview(HUDView.shared)
                UIView.animate(withDuration: Constants.animationDuration, animations: {
                    HUDView.shared.alpha = 1
                })
            }
        }
    }
    
    static func hideHUD() {
        if HUDView.shared.indicatorView.isAnimating {
            DispatchQueue.main.async {
                UIView.animate(withDuration: Constants.animationDuration, animations: {
                    HUDView.shared.alpha = 0
                }, completion: { (completed) in
                    HUDView.shared.indicatorView.stopAnimating()
                    HUDView.shared.removeFromSuperview()
                })
            }
        }
        
    }

}
