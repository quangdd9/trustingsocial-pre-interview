//
//  CycleStepView.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/12/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

@IBDesignable class CycleStepView: UIView {
    
    @IBInspectable var title: String = "1"
    
    @IBInspectable var done: Int = 0
    
    let greenColor = UIColor(named: "App theme")!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initViews()
    }
    
    private func initViews() {
        backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let cyclePath = UIBezierPath.init(arcCenter: CGPoint(x: bounds.width/2, y: bounds.height/2), radius: bounds.width/2 - 1, startAngle: 0, endAngle: CGFloat.pi*2, clockwise: true)
        cyclePath.lineWidth = 1
        greenColor.setStroke()
        if done <= 0 {
            UIColor.white.setFill()
        } else {
            greenColor.setFill()
        }
        cyclePath.fill()
        cyclePath.stroke()
        
        let paraGrapth = NSMutableParagraphStyle()
        paraGrapth.alignment = .center
        let attributeString = NSAttributedString(string: title, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 22), NSAttributedStringKey.foregroundColor: done <= 0 ? greenColor : UIColor.white, NSAttributedStringKey.paragraphStyle: paraGrapth])
        let fontHeight = UIFont.systemFont(ofSize: 22).lineHeight
        attributeString.draw(in: CGRect(x: 0, y: bounds.height/2 - fontHeight/2, width: bounds.width, height: fontHeight))
    }
}
