//
//  LoanInfo.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation

class LoanInfo {
    
    let amountMin: Int
    let amountMax: Int
    let tenorMin: Int
    let tenorMax: Int
    let interest: Double
    let bankName: String
    let bankLogoUrl: String
    
    init?(fromResponse response: [String: Any]) {
        guard let amountMin = response["min_amount"] as? Int,
        let amountMax = response["max_amount"] as? Int,
        let tenorMin = response["min_tenor"] as? Int,
        let tenorMax = response["min_tenor"] as? Int,
        let interest = response["interest_rate"] as? Double,
        let bank = response["bank"] as? [String: String],
        let bankName = bank["name"],
        let bankogo = bank["logo"] else { return nil }
        
        self.amountMin = amountMin
        self.amountMax = amountMax
        self.tenorMin = tenorMin
        self.tenorMax = tenorMax
        self.interest = interest
        self.bankName = bankName
        self.bankLogoUrl = bankogo
    }
    
// API Response
/*
{
    "min_amount": 30000000,
    "max_amount": 100000000,
    "min_tenor": 6,
    "max_tenor": 18,
    "interest_rate": 19.99,
    "bank": {
    }
    "name": "Vietcombank",
    "logo": "https://www.vietcombank.com.vn/images/logo30.png"
}
*/
    
}
