//
//  LoanInfoTableViewDataSource.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation
import UIKit

class LoanInfoTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    let loanInfo: LoanInfo
    
    init(loanInfo: LoanInfo) {
        self.loanInfo = loanInfo
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: LoanInfoCell.self)) as! LoanInfoCell
        let currencyFormat = NumberFormatter()
        currencyFormat.numberStyle = .currency
        currencyFormat.locale = Locale(identifier: "vi_VN")
        switch indexPath.row {
        case 0:
            cell.set(title: "Minimum amount", subTitle: currencyFormat.string(from: loanInfo.amountMin as NSNumber)!)
        case 1:
            cell.set(title: "Maximum amount", subTitle: currencyFormat.string(from: loanInfo.amountMax as NSNumber)!)
        case 2:
            cell.set(title: "Minimum tenor", subTitle: "\(loanInfo.tenorMin) months")
        case 3:
            cell.set(title: "Maximum tenor", subTitle: "\(loanInfo.tenorMax) months")
        case 4:
            cell.set(title: "Interest (per year)", subTitle: "\(loanInfo.interest) %")
        default:
            cell.set(title: "N/A", subTitle: "N/A")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title: String!
        let message: String!
        
        switch indexPath.row {
        case 0:
            title = "Minimum amount"
            message = "The minimum amount of loan you can get"
        case 1:
            title = "Maximum amount"
            message = "The maximum amount of loan you can get"
        case 2:
            title = "Minimum tenor"
            message = "The minimum tenor of your loan that would be calculated per month"
        case 3:
            title = "Minimum amount"
            message = "The maximum tenor of your loan that would be calculated per month"
        case 4:
            title = "Interest"
            message = "The interest rate of your loan that would be calculated per year"
        default:
            return
        }
        
        AlertPopupView.showAlert(withTitle: title, message: message, buttonTitle: "Got it!")
    }
    
}
