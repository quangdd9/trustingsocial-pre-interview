//
//  LoanInfoCell.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

class LoanInfoCell: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var viewSubContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        viewSubContent.layer.borderWidth = 1
        viewSubContent.layer.borderColor = UIColor(named: "App theme")!.cgColor
    }

    func set(title: String, subTitle: String) {
        lblTitle.text = title
        lblSubtitle.text = subTitle
    }
}
