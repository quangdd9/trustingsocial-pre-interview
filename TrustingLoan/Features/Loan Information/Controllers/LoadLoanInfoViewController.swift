//
//  ViewController.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/11/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

class LoadLoanInfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    func loadData() {
        HUDView.showHUD(title: "Retrieving Loan information ...")
        ServicesContainer.shared.loanServices().getLoanInfo(onSuccess: { (loanInfo) in
            HUDView.hideHUD()
            self.performSegue(withIdentifier: "showLoanDetail", sender: loanInfo)
        }) { (error) in
            HUDView.hideHUD()
            AlertPopupView.showAlert(withTitle: "Error!", message: error.localizedDescription, buttonTitle: "Try again", completion: {
                self.loadData()
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        let navigationController = segue.destination as! UINavigationController
        let vc = navigationController.viewControllers.first! as! LoanInfoViewController
        vc.loanInfo = sender as! LoanInfo
    }

}

