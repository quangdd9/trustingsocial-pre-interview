//
//  LoanInfoViewController.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

class LoanInfoViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var loanInfo: LoanInfo!
    var loanInfoDataSource: LoanInfoTableViewDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        loanInfoDataSource = LoanInfoTableViewDataSource(loanInfo: loanInfo)
        tableView.dataSource = loanInfoDataSource
        tableView.delegate = loanInfoDataSource
        tableView.register(UINib.init(nibName: String.init(describing: LoanInfoCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: LoanInfoCell.self))
        tableView.reloadData()
    }

}
