//
//  LoanCompletedViewController.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/16/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit
import SafariServices

class LoanCompletedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func supportAction(_ sender: UIButton) {
        let webView = SFSafariViewController(url: URL.init(string: Constants.supportURL)!)
        webView.preferredControlTintColor = UIColor(named: "App theme")!
        webView.dismissButtonStyle = .close
        let navigation = UINavigationController(rootViewController: webView)
        navigation.isNavigationBarHidden = true
        
        present(navigation, animated: true, completion: nil)
    }

}
