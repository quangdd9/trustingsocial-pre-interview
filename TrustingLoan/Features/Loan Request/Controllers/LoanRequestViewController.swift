//
//  LoanRequestViewController.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

class LoanRequestViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnSubmit: UIButton!
    
    var loanRequestFormDataSource: LoanRequestTableViewDataSource!
    var observation = [NSKeyValueObservation]()

    override func viewDidLoad() {
        super.viewDidLoad()

        loanRequestFormDataSource = LoanRequestTableViewDataSource()
        tableView.dataSource = loanRequestFormDataSource
        tableView.delegate = loanRequestFormDataSource
        tableView.register(UINib.init(nibName: String.init(describing: LoanInputCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: LoanInputCell.self))
        tableView.reloadData()

        loanRequestFormDataSource.isAllInputValid = { (isAllInputValid) in
            self.btnSubmit.isEnabled = isAllInputValid
        }
    }

    @IBAction func submitAction(sender: UIButton) {
        HUDView.showHUD(title: "Submitting your loan ...")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            HUDView.hideHUD()
            self.performSegue(withIdentifier: "showCompleted", sender: nil)
        }
    }

}
