//
//  LoanRequestTableViewDataSource.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation
import UIKit

class LoanRequestTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private let loanInputs:[LoanInputModel] = [LoanInputModel(loanInputType: .phone, isInputValid: false),
                                               LoanInputModel(loanInputType: .name, isInputValid: false),
                                               LoanInputModel(loanInputType: .idNumber, isInputValid: true),
                                               LoanInputModel(loanInputType: .province, isInputValid: false),
                                               LoanInputModel(loanInputType: .income, isInputValid: false)]
    
    private var observations = [NSKeyValueObservation]()
    
    var isAllInputValid: ((Bool) -> Void)? = nil {
        didSet {
            removeObervations()
            for loanInput in loanInputs {
                observations.append(loanInput.observe(\.isInputValid, changeHandler: { (_, _) in
                    let falseInputs = self.loanInputs.filter({ (loanInput) -> Bool in
                        return loanInput.isInputValid == false
                    })
                    self.isAllInputValid?(falseInputs.count == 0)
                }))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loanInputs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: LoanInputCell.self)) as! LoanInputCell
        cell.set(loanInputModel: loanInputs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! LoanInputCell
        cell.startEntering()
    }
    
    private func removeObervations() {
        for observer in observations {
            observer.removeObserver(observer, forKeyPath: "isInputValid")
        }
        observations.removeAll()
    }
    
    deinit {
        removeObervations()
    }
    
}
