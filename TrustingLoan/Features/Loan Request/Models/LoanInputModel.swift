//
//  LoanInputModel.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/18/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation

class LoanInputModel: NSObject {
    
    let loanInputType: LoanInputType
    @objc dynamic var isInputValid: Bool = false
    
    init(loanInputType: LoanInputType, isInputValid: Bool) {
        self.loanInputType = loanInputType
        self.isInputValid = isInputValid
    }
    
}
