//
//  LoanRequestForm.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation

class LoanRequetForm {
    
    let phone: String
    let name: String
    let idNumber: String?
    let province: String
    let monthlyIncome: Int
    
    init(phone: String, name: String, idNumber: String?, province: String, monthlyIncome: Int) {
        self.phone = phone
        self.name = name
        self.idNumber = idNumber
        self.province = province
        self.monthlyIncome = monthlyIncome
    }
    
}
