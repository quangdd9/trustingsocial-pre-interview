//
//  LoanInputCell.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import UIKit

enum LoanInputType {
    case phone, name, idNumber, province, income
}

class LoanInputCell: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var txtField: UITextField!
    @IBOutlet var viewSubContent: UIView!
    
    private let smallTitleFont = UIFont.systemFont(ofSize: 12)
    private let normalTitleFont = UIFont.systemFont(ofSize: 16)
    private var provinces: [String]!
    private lazy var incomes = ["Less than 3.000.000", "3.000.000 to 10.000.000", "Greater than 10.000.000"]
    private var loanInputModel: LoanInputModel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        viewSubContent.layer.borderWidth = 1
        viewSubContent.layer.borderColor = UIColor(named: "App theme")!.cgColor
        
        txtField.delegate = self
    }
    
    func set(loanInputModel: LoanInputModel) {
        self.loanInputModel = loanInputModel
        switch loanInputModel.loanInputType {
        case .phone:
            lblTitle.setRequired(text: "Phone number")
            txtField.keyboardType = .phonePad
        case .name:
            lblTitle.setRequired(text: "Full name")
            txtField.autocapitalizationType = .words
        case .idNumber:
            lblTitle.text = "National ID number"
            txtField.keyboardType = .numberPad
        case .province:
            lblTitle.setRequired(text: "Province")
        case .income:
            lblTitle.setRequired(text: "Monthly income")
        }
    }
    
    func startEntering() {
        switch loanInputModel.loanInputType {
        case .province:
            HUDView.showHUD(title: "Retrieving provinces ...")
            ServicesContainer.shared.loanServices().getProvinces(onSuccess: { (provinces) in
                HUDView.hideHUD()
                self.provinces = provinces
                let pickerView = UIPickerView()
                pickerView.dataSource = self
                pickerView.delegate = self
                self.txtField.inputView = pickerView
                self.txtField.becomeFirstResponder()
            }) { (error) in
                HUDView.hideHUD()
                AlertPopupView.showAlert(withTitle: "Error!", message: error.localizedDescription)
            }
            
        case .income:
            let pickerView = UIPickerView()
            pickerView.dataSource = self
            pickerView.delegate = self
            txtField.inputView = pickerView
            txtField.becomeFirstResponder()
            
        default:
            txtField.becomeFirstResponder()
        }
        
        
    }
    
    func show(errorText: String?) {
        guard let error = errorText else {
            switch loanInputModel.loanInputType {
            case .phone:
                lblTitle.setRequired(text: "Phone number")
            case .name:
                lblTitle.setRequired(text: "Full name")
            case .idNumber:
                lblTitle.text = "National ID number"
                lblTitle.textColor = UIColor.black
            case .province:
                lblTitle.setRequired(text: "Province")
            case .income:
                lblTitle.setRequired(text: "Monthly income")
            default:
                break
            }
            return
        }
        lblTitle.text = error
        lblTitle.textColor = UIColor.red
    }
}

extension LoanInputCell: UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text as NSString? else { return true }
        let updatedText = text.replacingCharacters(in: range, with: string)
        switch loanInputModel.loanInputType {
        case .phone:
            show(errorText: updatedText.isValidPhoneNumber() ? nil : "Phone number is incorrect!")
            loanInputModel.isInputValid = updatedText.isValidPhoneNumber()
        case .name:
            show(errorText: updatedText.isEmpty ? "Full name cannot be empty!" : nil)
            loanInputModel.isInputValid = !updatedText.isEmpty
        case .idNumber:
            show(errorText: updatedText.isValidNationalID() ? nil : "National ID number must be 9 or 12 digits!")
            loanInputModel.isInputValid = updatedText.isValidNationalID() || updatedText.isEmpty
        default:
            break
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.txtField.isHidden = self.txtField.text == ""
        self.lblTitle.font = self.txtField.text == "" ? self.normalTitleFont : self.smallTitleFont
        if txtField.text == "" {
            show(errorText: nil)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.txtField.isHidden = false
        self.lblTitle.font = self.smallTitleFont
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch loanInputModel.loanInputType {
        case .province:
            return provinces.count
        case .income:
            return incomes.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch loanInputModel.loanInputType {
        case .province:
            return provinces[row]
        case .income:
            return incomes[row]
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch loanInputModel.loanInputType {
        case .province:
            txtField.text = provinces[row]
            loanInputModel.isInputValid = true
        case .income:
            txtField.text = incomes[row]
            loanInputModel.isInputValid = true
        default:
            break
        }
    }
    
}
