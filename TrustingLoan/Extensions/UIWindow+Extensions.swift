//
//  UIWindow+Extensions.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/12/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {
    static var applicationWindow: UIWindow? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.window
    }
}
