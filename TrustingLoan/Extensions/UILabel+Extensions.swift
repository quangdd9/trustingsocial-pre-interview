//
//  UILabel+Extensions.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/16/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func setRequired(text: String) {
        let attributeString = NSMutableAttributedString(string: text, attributes: [NSAttributedStringKey.font : self.font, NSAttributedStringKey.foregroundColor: UIColor.black])
        let requireString = NSAttributedString(string: "*", attributes: [NSAttributedStringKey.font : self.font, NSAttributedStringKey.foregroundColor: UIColor.red])
        attributeString.append(requireString)
        self.attributedText = attributeString
    }
    
}
