//
//  CALayer+Extensions.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/12/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    // For interface builder using
    var borderUIColor: UIColor {
        set {
            borderColor = newValue.cgColor
        }
        
        get {
            return UIColor.init(cgColor: borderColor!)
        }
    }
    
}
