//
//  MockLoanServices.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/19/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation

class MockLoanServices: LoanServiceProtocol {
    
    func getLoanInfo(onSuccess: @escaping (LoanInfo) -> Void, onError: @escaping (Error) -> Void) {
        
        // Creating mock data
        let mockResponse: [String : Any] = ["min_amount": 30000000,
                            "max_amount": 100000000,
                            "min_tenor": 3,
                            "max_tenor": 24,
                            "interest_rate": 12.9,
                            "bank": ["name": "Vietcombank", "logo": "https://www.vietcombank.com.vn/images/logo30.png"]]
        let loanInfo = LoanInfo(fromResponse: mockResponse)!
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            onSuccess(loanInfo)
        }
    }
    
    func getProvinces(onSuccess: @escaping ([String]) -> Void, onError: @escaping (Error) -> Void) {
        
        // Creating mock data
        let mockResponse = ["Test 1", "Test 2", "Test 3", "Test 4", "Test 5"]
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            onSuccess(mockResponse)
        }
    }
    
    deinit {
        print("Service has been released!")
    }
    
}

/*
{
    "min_amount": 30000000,
    "max_amount": 100000000,
    "min_tenor": 6,
    "max_tenor": 18,
    "interest_rate": 19.99,
    "bank": {
        "name": "Vietcombank",
        "logo": "https://www.vietcombank.com.vn/images/logo30.png"
}
*/
 
 /*
 {
    "total": 10,
    "provinces_list": [
        "An Giang",
        "Bắc Giang",
        "Bắc Kạn",
        "Bạc Liêu",
        "Bắc Ninh",
        "Bà Rịa - Vũng Tàu",
        "Bến Tre",
        "Bình Định",
        "Bình Dương",
        "Bình Phước"]
 }
 */

