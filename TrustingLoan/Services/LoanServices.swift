//
//  LoanServices.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/14/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation
import Alamofire

class LoanServices: LoanServiceProtocol {
    
    func getLoanInfo(onSuccess: @escaping (LoanInfo) -> Void, onError: @escaping (Error) -> Void) {
        let url = Constants.baseURL + Constants.getLoanInfoPath
        Alamofire.request(url).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                guard let data = data as? [String: Any], let model = LoanInfo(fromResponse: data) else {
                    onError(NSError.init(domain: "", code: 404, userInfo: nil))
                    return
                }
                onSuccess(model)
                
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func getProvinces(onSuccess: @escaping ([String]) -> Void, onError: @escaping (Error) -> Void) {
        let url = Constants.baseURL + Constants.getProvincesPath
        Alamofire.request(url).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                guard let data = data as? [String: Any], let provinces = data["provinces_list"] as? [String] else {
                    onError(NSError.init(domain: "", code: 404, userInfo: nil))
                    return
                }
                onSuccess(provinces)
                
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    deinit {
        print("Service has been released!")
    }
    
}
