//
//  LoanServiceProtocol.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/19/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation

protocol LoanServiceProtocol {
    
    func getLoanInfo(onSuccess: @escaping (LoanInfo) -> Void, onError: @escaping (Error) -> Void)
    func getProvinces(onSuccess: @escaping ([String]) -> Void, onError: @escaping (Error) -> Void)
    
}
