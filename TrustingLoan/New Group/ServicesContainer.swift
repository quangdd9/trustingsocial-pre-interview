//
//  ServicesContainer.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/19/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation
import Swinject

class ServicesContainer: NSObject {
    
    static let shared = ServicesContainer()
    private let container: Container
    
    override init() {
        container = Container()
        container.register(LoanServiceProtocol.self) { _ in
            return LoanServices()
        }
    }
    
    func loanServices() -> LoanServiceProtocol {
        return container.resolve(LoanServiceProtocol.self)!
    }
    
}
