//
//  Constants.swift
//  TrustingLoan
//
//  Created by QuangDao on 9/12/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import Foundation

class Constants {
    // App constants
    static let animationDuration = 0.25
    static let supportURL = "http://www.vietcombank.com.vn/EBanking/PhoneBanking"
    
    // Base URLs
    static let baseURL = "https://km50-9538e.firebaseio.com"
    
    // Paths
    static let getLoanInfoPath = "/offer.json"
    static let getProvincesPath = "/provinces.json"
    static let submitLoanPath = "/loans.json"
}
