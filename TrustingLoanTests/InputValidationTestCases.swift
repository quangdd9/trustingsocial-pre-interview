//
//  InputValidationTestCases.swift
//  TrustingLoanTests
//
//  Created by QuangDao on 9/19/18.
//  Copyright © 2018 QuangDao. All rights reserved.
//

import XCTest

class InputValidationTestCases: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPhoneValidation() {
        let mockPassDatas = ["0908330570", "01632652615", "0912349876"]
        let mockFailedDatas = ["090", "0", "0163265", "abc", "abc1234", ""]
        
        let passCases = mockPassDatas.filter { (mock) -> Bool in
            return mock.isValidPhoneNumber()
        }
        let failedCases = mockFailedDatas.filter { (mock) -> Bool in
            return !mock.isValidPhoneNumber()
        }
        
        XCTAssert(passCases.count == mockPassDatas.count && failedCases.count == mockFailedDatas.count, "Phone validation has been failed!")
    }
    
    func testNationalIDValidation() {
        let mockPassDatas = ["187033948", "198755656256", "000000000", "999999999999"]
        let mockFailedDatas = ["123", "0", "0163265", "abc", "abc1234", "", "zxcvasd12"]
        
        let passCases = mockPassDatas.filter { (mock) -> Bool in
            return mock.isValidNationalID()
        }
        let failedCases = mockFailedDatas.filter { (mock) -> Bool in
            return !mock.isValidNationalID()
        }
        
        XCTAssert(passCases.count == mockPassDatas.count && failedCases.count == mockFailedDatas.count, "National ID validation has been failed!")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
